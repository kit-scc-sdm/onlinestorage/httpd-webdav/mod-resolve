# mod_resolve

This module adds integration points to call external programs to resolve values and perform other logic during Apache HTTPD request processing.

At the moment, the only supported resolution method is starting a process from a program with arbitrary variables as arguments.
Caching can offset this relatively expensive operation.

## Configuration

### `ResolveByProcess`

`ResolveByProcess <name> none|[<key>=<value>[,<key>=<value>...]] <executable_path> <arg_expr> <arg_expr>...`

Contexts: virtual host, directory

Creates and runs a process to resolve a variable.
The executable/program is invoked after evaluating the specified arguments as [string expressions](https://httpd.apache.org/docs/2.4/expr.html) and passing the result as arguments to the process.

For security reasons, the executable path is not interpreted as an expression.
Also, the full path to the executable has to be given as `$PATH` will not be searched.
The arguments are split while reading the configuration and no further splitting takes place after evaluation.
Hence, there is full control over passing parameters to the process.
When in doubt, double quote individual parameters in the configuration to ensure proper parsing.

The standard output of the process is taken as the resolved value.
Whitespace around the value is stripped, for example a trailing newline.
The standard error is logged line wise into the HTTPD logs, at the moment with the hard-coded level `WARNING`.
If the process exits with a not-0 exit code, the request processing is aborted with a 500 Internal Server Error HTTP response.

It is generally expected to resolve via scripts/programs specifically written to match this interface, but it may also be possible to call existing programs.
No environment is passed to the process and `SetEnv` etc. is ignored.

Several optional parameters can be specified to configure the resolution in detail.
The following parameters are available:

 * `stage`: The stage during request processing at which the resolution takes place.
   See section [Stages](#stages).
   Default is `post_read_request:middle`.
 * `destination`: The place where the resolved value will be stored.
   See section [Destinations](#destinations).
   Default is `reqenv`.
 * `cache`: The name of a registered cache to enable caching of the process result.
   See section [Caching](#caching).
   Default is no caching.
 * `cache_ttl`: Duration for which to keep each individual entry in the cache.
   The numeric value must have a postfix `ms`/`s`/`m`/`h`.
   Default is 0.

The directive can be specified multiple times to resolve multiple values.
The order in which several variables in the same stage are resolved is arbitrary and there is no way to influence this.
When specifying the resolution of the same variable (destination + name) at a more specific directory, only the more specific resolution will take place.
This overwriting fully replaces the more general configuration without inheriting any parameters.

Enabling debug logging in HTTPD (`LogLevel`) will print additional log messages explaining what is resolved when and to which value.
This log level may print user information and secrets and should only be used during testing.

#### Stages

The stages for variable resolution are built on the [hook system](https://httpd.apache.org/docs/2.4/developer/hooks.html) of HTTPD.

The following stages are available:

 * `fixups:middle`: This is a hook registered for `ap_hook_fixups` with `APR_HOOK_MIDDLE`.
 * `post_read_request:middle`: This is a hook registered for `ap_hook_post_read_request` with `APR_HOOK_MIDDLE`.
 * `check_authn:after`: This is a hijacking hook which is executed after the regular processing of `ap_hook_check_authn`/`ap_hook_check_user_id` has finished.
   It is useful for mapping a user authenticated by a module such as `mod_auth_basic` or `mod_oauth2` to another user or making further user information available.
 * `check_authz:after`: This is a hijacking hook which is executed after the regular processing of `ap_hook_check_authz`/`ap_hook_auth_checker`.

If you suspect problems with ordering, set an environment variable `SHOW_HOOKS=1` when starting HTTPD to have HTTPD print the invocation order of modules per hook.

For subrequests, the set of executed hooks differs and some stages may not be executed again reusing the value from the main request.
Authn/Authz hooks are running in `AP_AUTH_INTERNAL_PER_CONF` mode meaning they will not be re-executed for a subrequest having the same authentication and authorization configuration as the main request.

#### Destinations

Destinations control where exactly the resolved value is stored.

 * `reqenv`: Sets a field in the request environment with the name specified in the directive.

 * `field`: Sets a "special" named field in the request.
   The name specified in the directive must match a known field name.
   These fields are available:

   * `REMOTE_USER`: The username associated with the current request.

#### Examples

```
# Map the "sub" claim from an OAuth2 access token parsed by mod_oauth2 to a
# username.
# Uses the cache "default" and the access token is passed as an argument to
# ensure the script is called anew whenever the access token is rotated.
ResolveByProcess REMOTE_USER destination=field,stage=check_authn:after,cache=default,cache_ttl=1h /usr/local/bin/map-to-username %{reqenv:OAUTH2_CLAIM_sub} %{reqenv:OAUTH2_CLAIM_access_token}
```

### Authorization (Authz) Provider

The module registers the `by-process` provider for authorization decisions.
The provider is used with the [`Require` directive](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require):

`Require by-process none|[<key>=<value>[,<key>=<value>] <executable_path> <arg_expr> <arg_expr>...`

The configuration works in the same ways as `ResolveByProcess`.
Check the `ResolveByProcess` documentation for details information.
Only the following parameters are available:

 * `cache`
 * `cache_ttl`

The process must write an authorization result to stdout.
That is one of the following keywords:

 * `granted`: The request is granted.
 * `denied`: The request is denied.
 * `neutral`: The request is neither granted nor denied.
 * `denied_no_user`: The request is denied but may be granted if user information associated with the request is made available to the program.

The process may be executed several times for a single request.
E.g. without user information and if returning `denied_no_user` it will be called again with user information.

If the process exits with a not-0 exit code, the provider returns a `AUTHZ_GENERAL_ERROR`.
However, the debug log message `AH01626` states that the result was `neutral`, but this is due to output formatting.

### Caching

The result of a process execution can be stored in a cache and subsequent resolutions can use the cached result, skipping process execution.
`mod_resolve` relies on the [socache](https://httpd.apache.org/docs/2.4/socache.html) mechanism for caching which is built into HTTPD.
With socache, cache providers are made available as modules.

This module has been tested with the [shmcb](https://httpd.apache.org/docs/2.4/mod/mod_socache_shmcb.html) and the [redis](https://httpd.apache.org/docs/2.4/mod/mod_socache_redis.html) providers.
The shmcb provider is recommended in particular because it works well in a variety of scenarios, has no dependency on the network and is very fast.

The combination of all input parameters is used as the cache key.
For process execution, this is the path to the executable along with all arguments.
In addition, all cache entries have a limited time-to-live (TTL) which is set when inserting into the cache.
There is no way to invalidate cache entries beyond relying on the time-to-live.
If there is a need to re-execute a process based on a non-input variable, this variable should be added as an argument which is ignored by the process.
For example, a resolving a value based on a field in an OAuth2 access token can be repeated on access token rotation by adding the entire access token as a parameter.

#### `ResolveCache`

`ResolveCache <name> <provider_name>[:<provider_args>]`

Context: server config

Registers a named cache which can be used with the `cache` parameter in `ResolveByProcess` and similar configuration directives.
The cache will store the results of evaluations, for example process executions.

A single register cache can be used in any number of directives.

An example with `mod_socache_shmcb`:

```
# Register a cache
ResolveCache mycache shmcb:/var/run/mod_resolve(512000)

<VirtualHost ...>
    # ...

    # Use the registered cache
    ResolveByProcess geo_region stage=fixups:middle,cache=mycache,cache_ttl=1h /usr/local/bin/code-to-region %{req_novary:X-Geo-Code}
</VirtualHost>
```

## Runtime Dependencies

There are no special runtime dependencies.

The module requires the Apache Portable Runtime (APR) >= v1.4.0.

## Releases, Packaging and Distribution

`mod_resolve` is released through tags in the Git repository.
The versioning scheme follows [Semantic Versioning 2.0.0](https://semver.org/).

RPMs are built in GitLab CI and made available as job artifacts.
These artifacts can be downloaded from the "Tags" page in GitLab by clicking the download icon next to a version tag and selecting the appropriate job, e.g. `package:rpm:el8` for Enterprise Linux 8.
Versions of the RPMs correspond to the versions of the module.

A module builder Docker image is also built, but there is no tagging in place yet.
See the test configurations for how to use the image and see the Docker Basics repository for background.

### For Maintainers: Releasing

Releasing a new version requires amending the version in several places:

 * `rpms/httpd-mod-resolve.spec`: `upstream_version`

These changes have to be committed and this commit tagged with `v${VERSION}`.

## Developing and Testing

Docker images are built for development and testing.
See the Docker Basics repository for background.

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the Apache License, Version 2.0 (see LICENSE file).
Detailed authorship information is part of the Git commit data or included in the files directly.

```
Copyright 2022-2023 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
