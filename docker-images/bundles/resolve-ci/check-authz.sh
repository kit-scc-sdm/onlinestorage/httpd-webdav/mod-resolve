#!/bin/sh

echo "check-authz.sh called with cmd line: $0 $@" >&2

if [ -z "$1" ]; then
	echo denied_no_user
	exit 0
fi

if [ "$2" = "secret-GIOL3PMJ4B" ]; then
	echo granted
	exit 0
else
	echo denied
	exit 0
fi
