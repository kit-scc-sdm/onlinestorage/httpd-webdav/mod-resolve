import pytest

def test_basic(httpd_base_url, session):
    r = session.get(httpd_base_url / 'index.html')
    assert r.status_code == 401
    assert 'X-Fullname' not in r.headers

    r = session.get(httpd_base_url / 'index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert r.headers['X-Fullname'] == 'Test User'

    r = session.get(httpd_base_url / 'index.html', auth=('unknown-user', 'unknown-password'))
    assert r.status_code == 200
    assert r.headers['X-Fullname'] == '[full name unknown]'

def test_overwriting(httpd_base_url, session):
    r = session.get(httpd_base_url / 'overwriting/index.html')
    assert r.status_code == 401
    assert 'X-Overwriting-Var' not in r.headers

    r = session.get(httpd_base_url / 'overwriting/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert r.headers['X-Overwriting-Var'] == 'Test User'

    r = session.get(httpd_base_url / 'overwriting/subdir/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert r.headers['X-Overwriting-Var'] == '1'

def test_field(httpd_base_url, session):
    r = session.get(httpd_base_url / 'field/index.html')
    assert r.status_code == 401
    assert 'X-User' not in r.headers

    r = session.get(httpd_base_url / 'field/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert r.headers['X-User'] == 'Test User'

    r = session.get(httpd_base_url / 'field/index.html', auth=('unknown-user', 'unknown-password'))
    assert r.status_code == 200
    assert r.headers['X-User'] == '[full name unknown]'

def test_authz(httpd_base_url, session):
    r = session.get(httpd_base_url / 'authz/index.html')
    assert r.status_code == 401

    r = session.get(httpd_base_url / 'authz/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 401

    r = session.get(httpd_base_url / 'authz/index.html', auth=('test-user', 'test-password'), headers={'X-Secret': 'not correct'})
    assert r.status_code == 401

    r = session.get(httpd_base_url / 'authz/index.html', auth=('test-user', 'test-password'), headers={'X-Secret': 'secret-GIOL3PMJ4B'})
    assert r.status_code == 200

    r = session.get(httpd_base_url / 'authz/index.html', headers={'X-Secret': 'secret-GIOL3PMJ4B'})
    assert r.status_code == 401

def test_cache(httpd_base_url, session):
    r = session.get(httpd_base_url / 'cache/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert len(r.headers.get('X-Date', '')) > 0
    test_user_date = r.headers['X-Date']

    r = session.get(httpd_base_url / 'cache/index.html', auth=('test-user', 'test-password'))
    assert r.status_code == 200
    assert r.headers['X-Date'] == test_user_date

    r = session.get(httpd_base_url / 'cache/index.html', auth=('unknown-user', 'unknown-password'))
    assert r.status_code == 200
    assert len(r.headers.get('X-Date', '')) > 0
    assert r.headers['X-Date'] != test_user_date
    unknown_user_date = r.headers['X-Date']

    r = session.get(httpd_base_url / 'cache/index.html', auth=('unknown-user', 'unknown-password'))
    assert r.status_code == 200
    assert r.headers['X-Date'] == unknown_user_date
