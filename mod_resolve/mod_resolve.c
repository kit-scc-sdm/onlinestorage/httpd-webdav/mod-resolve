#define APR_WANT_STRFUNC
#define APR_WANT_MEMFUNC
#define APR_WANT_BYTEFUNC
#include "apr_want.h"
#include "apr_thread_proc.h"
#include "apr_file_io.h"
#include "apr_errno.h"
#include "apr_poll.h"
#include "apr_strings.h"
#include "apr_tables.h"
#include "apr_time.h"
#include "httpd.h"
#include "http_log.h"
#include "http_config.h"
#include "http_request.h"
#include "http_protocol.h"
#include "ap_config.h"
#include "ap_expr.h"
#include "ap_provider.h"
#include "ap_socache.h"
#include "mod_auth.h"

// APACHE_ARG_MAX
#include "util_script.h"

// I'm uncertain where APR_HAVE_STDLIB_H is actually defined
#if APR_HAVE_STDLIB_H
// For strtoll
#include <stdlib.h>
#else
// TODO: What to do if stdlib is not available?
// There is also an apr_strtoi64() function part of APR which may replace strtoll.
// In APR >= 1.6.0 there is also apr_cstr.h defining apr_cstr_atoui64().
#endif

// Work-around for APR < 1.6.0: APR_ERANGE is not defined so it will be defined in this file.
#ifndef APR_ERANGE

#ifdef ERANGE
#define APR_ERANGE   ERANGE
#else
// The actual definition of APR_ERANGE happens below in the error space of mod_resolve.
#define RSLV_ERANGE_WORKAROUND
#endif

#define APR_STATUS_IS_ERANGE(s)   ((s) == APR_ERANGE)
#endif

// Declare module, data is filled in at the end of the file.
module AP_MODULE_DECLARE_DATA resolve_module;

/**
 * Size of the buffers used for I/O operations, mostly reading from stdout and
 * stderr of running processes.
 *
 * This happens to be PIPE_BUF on Linux, see pipe(7).
 */
#define READ_BUF_SIZE 4096

/**
 * Size of the buffer allocated for getting the error string for a status via
 * strerror, apr_strerror or rslv_strerror.
 */
#define ERROR_STRING_BUF_SIZE 256

/**
 * Custom error space built on top of APR's error codes in the range [301500, 301600).
 */
#define RSLV_START_ERROR (APR_OS_START_USERERR + 301500)
#define RSLV_ERROR_SPACE_SIZE 100

// Custom error/status values:

/** The process died uncleanly, for example was killed by a signal. */
#define RSLV_UNCLEAN_PROCESS_DEATH   (RSLV_START_ERROR + 1)
/** The process/... produced more output than capacity was allocated.
 *  This did not actually lead to writing to invalid memory but just to discarding of output.
 */
#define RSLV_OUTPUT_OVERFLOW   (RSLV_START_ERROR + 2)
/** The process exited with an error exit code.
 */
#define RSLV_ERROR_PROCESS_EXIT_CODE   (RSLV_START_ERROR + 3)
/** An error occurred during evaluation of a expression (ap_expr) supplied via configuration.
 *  There should be an accompanying log message giving details.
 */
#define RSLV_EXPR_ERROR   (RSLV_START_ERROR + 4)
/** A syntax error was found during parsing of a "params" string in the configuration.
 */
#define RSLV_PARAMS_PARSING_ERROR   (RSLV_START_ERROR + 5)
/** The value of the passed enum or the passed name describing an enum is invalid.
 */
#define RSLV_INVALID_ENUM_VALUE   (RSLV_START_ERROR + 6)
/** The referred to cache is not known.
 */
#define RSLV_UNKNOWN_CACHE   (RSLV_START_ERROR + 7)
/** Invalid postfix (should be "ms", "s", ...) for duration values.
 */
#define RSLV_DURATION_INVALID_POSTFIX   (RSLV_START_ERROR + 8)
/** The success status is the same as APR_SUCCESS, but also signals to the
 *  caller that the file descriptor processed by the function should be
 *  removed from the pollset.
 */
#define RSLV_SUCCESS_REMOVE_DESCRIPTOR   (RSLV_START_ERROR + 51)

#ifdef RSLV_ERANGE_WORKAROUND
/**
 * Numeric value not representable.
 */
#define APR_ERANGE   (RSLV_START_ERROR + RSLV_ERROR_SPACE_SIZE - 1)
#endif

/**
 * Value to be returned by hook functions encountering an error.
 *
 * The chosen value is 500, so that if the return value is interpreted as a
 * HTTP status code "500 Internal Server Error" is responded to the client.
 */
#define RSLV_HOOK_ERROR 500

static char *rslv_status_to_string(apr_status_t status) {
    if (status < RSLV_START_ERROR || status >= RSLV_START_ERROR + RSLV_ERROR_SPACE_SIZE)
        return "Status not in the range of mod_resolve, no error string known";

    switch (status) {
    case RSLV_UNCLEAN_PROCESS_DEATH:
        return "Process died uncleanly";
    case RSLV_OUTPUT_OVERFLOW:
        return "Process/... produced more output than capacity was allocated";
    case RSLV_ERROR_PROCESS_EXIT_CODE:
        return "Process exited with error exit code";
    case RSLV_EXPR_ERROR:
        return "Error during evaluation of an expression (via ap_expr)";
    case RSLV_PARAMS_PARSING_ERROR:
        return "Error during parsing of a \"params\" string (syntax error)";
    case RSLV_INVALID_ENUM_VALUE:
        return "Value or name of the enum is invalid";
    case RSLV_UNKNOWN_CACHE:
        return "Unknown cache";
    case RSLV_DURATION_INVALID_POSTFIX:
        return "Invalid postfix for duration values";
    case RSLV_SUCCESS_REMOVE_DESCRIPTOR:
        return "Success";
#ifdef RSLV_ERANGE_WORKAROUND
    case APR_ERANGE:
        return "Numeric value not representable";
#endif
    default:
        return "Error string not specified yet";
    }
}

static char *rslv_strerror(apr_status_t status, char *buf, apr_size_t bufsize) {
    if (status < RSLV_START_ERROR || status >= RSLV_START_ERROR + RSLV_ERROR_SPACE_SIZE) {
        return apr_strerror(status, buf, bufsize);
    } else {
        apr_cpystrn(buf, rslv_status_to_string(status), bufsize);
        return buf;
    }
}

static const char *rslv_strerror_pool(apr_pool_t *pool, apr_status_t status) {
    char *buf = (char *) apr_palloc(pool, ERROR_STRING_BUF_SIZE);
    ap_assert(buf != NULL);
    rslv_strerror(status, buf, ERROR_STRING_BUF_SIZE);
    return buf;
}

typedef enum {
    RSLV_STDOUT,           /**< stdout of the process */
    RSLV_STDERR,           /**< stderr of the process */
} stream_e;

const char *stream_e_to_name(stream_e value) {
    switch (value) {
        case RSLV_STDOUT: return "STDOUT";
        case RSLV_STDERR: return "STDERR";
    }
    return "[invalid value]";
}

typedef enum {
    RSLV_FIXUPS_MIDDLE,              /**< fixups:middle */
    RSLV_POST_READ_REQUEST_MIDDLE,   /**< post_read_request:middle */
    RSLV_CHECK_AUTHN_AFTER,          /**< check_authn:after */
    RSLV_CHECK_AUTHZ_AFTER,          /**< check_authz:after */
} stage_e;

const char *stage_e_to_name(stage_e value) {
    switch (value) {
        case RSLV_FIXUPS_MIDDLE: return "fixups:middle";
        case RSLV_POST_READ_REQUEST_MIDDLE: return "post_read_request:middle";
        case RSLV_CHECK_AUTHN_AFTER: return "check_authn:after";
        case RSLV_CHECK_AUTHZ_AFTER: return "check_authz:after";
    }
    return "[invalid value]";
}

apr_status_t name_to_stage_e(const char *name, stage_e *stage) {
    if (strcmp(name, "fixups:middle") == 0) {
        *stage = RSLV_FIXUPS_MIDDLE;
        return APR_SUCCESS;
    } else if (strcmp(name, "post_read_request:middle") == 0) {
        *stage = RSLV_POST_READ_REQUEST_MIDDLE;
        return APR_SUCCESS;
    } else if (strcmp(name, "check_authn:after") == 0) {
        *stage = RSLV_CHECK_AUTHN_AFTER;
        return APR_SUCCESS;
    } else if (strcmp(name, "check_authz:after") == 0) {
        *stage = RSLV_CHECK_AUTHZ_AFTER;
        return APR_SUCCESS;
    } else {
        return RSLV_INVALID_ENUM_VALUE;
    }
}

typedef enum {
    RSLV_DESTINATION_REQENV,   /**< reqenv, store in the "table" r->subprocess_env */
    RSLV_DESTINATION_FIELD,    /**< field, named fields on request_rec *r */
} destination_e;

const char *destination_e_to_name(destination_e value) {
    switch (value) {
        case RSLV_DESTINATION_REQENV: return "reqenv";
        case RSLV_DESTINATION_FIELD: return "field";
    }
    return "[invalid value]";
}

apr_status_t name_to_destination_e(const char *name, destination_e *destination) {
    if (strcmp(name, "reqenv") == 0) {
        *destination = RSLV_DESTINATION_REQENV;
        return APR_SUCCESS;
    } else if (strcmp(name, "field") == 0) {
        *destination = RSLV_DESTINATION_FIELD;
        return APR_SUCCESS;
    } else {
        return RSLV_INVALID_ENUM_VALUE;
    }
}

typedef enum {
    RSLV_FIELD_REMOTE_USER,   /**< REMOTE_USER, r->user */
} field_e;

const char *field_e_to_name(field_e value) {
    switch (value) {
        case RSLV_FIELD_REMOTE_USER: return "REMOTE_USER";
    }
    return "[invalid value]";
}

apr_status_t name_to_field_e(const char *name, field_e *field) {
    if (strcmp(name, "REMOTE_USER") == 0) {
        *field = RSLV_FIELD_REMOTE_USER;
        return APR_SUCCESS;
    } else {
        return RSLV_INVALID_ENUM_VALUE;
    }
}

apr_status_t name_to_authz_status(const char *name, authz_status *status) {
    if (strcmp(name, "denied") == 0) {
        *status = AUTHZ_DENIED;
        return APR_SUCCESS;
    } else if (strcmp(name, "granted") == 0) {
        *status = AUTHZ_GRANTED;
        return APR_SUCCESS;
    } else if (strcmp(name, "neutral") == 0) {
        *status = AUTHZ_NEUTRAL;
        return APR_SUCCESS;
    } else if (strcmp(name, "authz_general_error") == 0) {
        *status = AUTHZ_GENERAL_ERROR;
        return APR_SUCCESS;
    } else if (strcmp(name, "denied_no_user") == 0) {
        *status = AUTHZ_DENIED_NO_USER;
        return APR_SUCCESS;
    } else {
        return RSLV_INVALID_ENUM_VALUE;
    }
}

const char *authz_status_to_name(authz_status value) {
    switch (value) {
        case AUTHZ_DENIED: return "denied";
        case AUTHZ_GRANTED: return "granted";
        case AUTHZ_NEUTRAL: return "neutral";
        case AUTHZ_GENERAL_ERROR: return "authz_general_error";
        case AUTHZ_DENIED_NO_USER: return "denied_no_user";
    }
    return "[invalid value]";
}

typedef enum {
    RSLV_CM_NONE,                /**< none */
    RSLV_CM_CACHE,               /**< cache */
    RSLV_CM_CACHE_UNINITIALISED, /**< cache (uninitialised) */
} caching_mode_e;

const char *caching_mode_e_to_name(caching_mode_e value) {
    switch (value) {
        case RSLV_CM_NONE: return "none";
        case RSLV_CM_CACHE: return "cache";
        case RSLV_CM_CACHE_UNINITIALISED: return "cache (uninitialised)";
    }
    return "[invalid value]";
}

/*
 * Hook Hijacking
 *
 * mod_resolve can hijack some hooks to be able gain more control over their
 * execution.
 *
 * Specifically, the problem addressed is that hooks with RUN_FIRST semantic
 * will execute registered hook functions in sort order only until the first
 * time OK is returned. It is not possible to run anything after the main
 * logic, because the first "main logic" hook function will lead to hook
 * execution to be stopped. Examples are the authentication and authorization
 * hooks.
 *
 * Hijacking works by registering a hijacking hook function in REALLY_FIRST
 * position, which then itself executes the hook via ap_run_*. This way, any
 * code can be called in the hijacking hook function after the inner, regular
 * hook execution has finished. Care must be taken to disable the hijacking
 * hook function to prevent a loop. This is done by storing a special pointer
 * value in a key of r->notes upon first entry of the hook function. If the
 * key is present and has this special pointer value, the hijacking hook
 * function does nothing.
 */

/*
 * Caching
 *
 * mod_resolve can store intermediate results in a cache for a limited amount
 * of time. Most importantly, evaluate_process() is wrapped by a cache
 * (by evaluate_resolve_config_by_process()) so that process execution does
 * not have to be repeated for each request.
 *
 * Cache key and scope
 *
 * The cache key must encode exactly the information required to reproduce the
 * value via the value's producing mechanism. For process execution, this is
 * the program name and its (fully evaluated) arguments. The cached value
 * encompasses the result of the execution consisting of the process' exit
 * code along with the process' stdout output. The process must
 * deterministically reproduce the same result for the set of inputs to
 * properly apply caching.
 *
 * Expiry
 *
 * Each cache entry has a defined TTL, although the entry may disappears
 * before this time is up. At the moment, there is a single TTL defined for
 * each ResolveByProcess directive and this is statically applied when
 * storing an entry in the cache.
 *
 * Serialisation of process result
 *
 * For storing the process result (evaluate_process_result_t) as a value in
 * the cache, the struct must be serialised. A simple custom serialisation
 * format is used, implemented by evaluate_process_result_marshal.
 */

/** Key name in r->notes indicating whether hijacking of the check_authn
  * (check_user_id) hook has occurred */
#define RSLV_AUTHN_HIJACK_STATE "mod_resolve:authn_hijack_state"
/** Key name in r->notes indicating whether hijacking of the check_authz
  * (auth_checker) hook has occurred */
#define RSLV_AUTHZ_HIJACK_STATE "mod_resolve:authz_hijack_state"

/**
 * The special private pointer which is used as a value in r->notes to
 * indicate that a particular hook was hijacked.
 *
 * This must be a unique, reliable pointer.
 *
 * FUTR: This can probably achieved in a better way, e.g. via an enum or at
 * least by using a (void *) as a type.
 */
static const char *hijack_state_hijacked = "mod_resolve:*_hijack_state=hijacked";

/**
 * Represents a cache registered under a name.
 *
 * The struct encompasses both provider and instance which suffices to perform all cache operations.
 */
typedef struct {
    const char *name;
    ap_socache_provider_t *provider;
    ap_socache_instance_t *instance;
} cache_t;

/**
 * Fully specifies a resolve configuration.
 * It is used to store and communicate the parameters between functions of this module.
 */
typedef struct {
    /** Unique key of the resolve configuration, describing the "target" where the resolved value is written.
     *  It is primarily used for deciding whether to overwrite a resolve configuration in a more-specific configuration file.
     *  At the moment this is built from destination and var_name.
     */
    const char *unique_key;
    /** var_name is the variable name given by the user in the configuration file and should be used in output. */
    const char *var_name;
    /** Stage during request processing at which to perform the resolve operation. */
    stage_e stage;
    /** Describes where the resolved value is written (reqenv, field on request_rec, ...). */
    destination_e destination;
    /** Field of the request_rec where the resolved value is written, if destination is field.  */
    field_e field;
    /** Full path to the executable, if resolving by process. */
    const char *prog_name;
    /** Array of expressions to be evaluated and passed as arguments to the process, if resolving by process.
     *  The first argument (index 0) passed to the process - which conventionally contains the executable name - must be prepended to this array.
     *  The array is not NULL terminated but its length is given by param_exprs_count.
     */
    ap_expr_info_t **param_exprs;
    /** Length of array param_exprs, if resolving by process. */
    apr_size_t param_exprs_count;

    caching_mode_e caching_mode;
    const char *cache_name;
    cache_t *cache;
    apr_time_t cache_ttl;
} resolve_config_t;

typedef struct {
    /** Array of all registered resolve configs (resolve_config_t *). */
    apr_array_header_t *resolves;
    /** Array of registered caches (cache_t *). */
    apr_array_header_t *caches;
} module_global_config_t;

typedef struct {
} module_server_config_t;

typedef struct {
    apr_array_header_t *fixups_middle_resolves;
    apr_array_header_t *post_read_request_middle_resolves;
    apr_array_header_t *check_authn_last_resolves;
    apr_array_header_t *check_authz_last_resolves;
} module_dir_config_t;

static module_global_config_t global_config;

static void str_trim_in_place(char *inp) {
    const char *trim_set = " \t\n\r";

    char trim_table[256] = {0};
    for (const char *trim_set_it = trim_set; *trim_set_it != '\x00'; ++trim_set_it) {
        trim_table[*trim_set_it] = 1;
    }

    const char *read_offset = inp;
    char *write_offset = inp;
    char *last_non_trim_set = write_offset - 1;

    // FUTR: Could skip ahead if > 127 for UTF-8 compatibility

    if (!trim_table[*read_offset]) {
        for (; *write_offset != '\x00'; ++write_offset) {
            if (!trim_table[*write_offset])
                last_non_trim_set = write_offset;
        }
    } else {
        for (; *read_offset != '\x00'; ++read_offset) {
            if (!trim_table[*read_offset])
                break;
        }

        for (; *read_offset != '\x00'; ++read_offset) {
            *write_offset = *read_offset;
            if (!trim_table[*write_offset])
                last_non_trim_set = write_offset;
            ++write_offset;
        }
    }

    *(last_non_trim_set + 1) = '\x00';
}

/**
 * Contains a chunk of output from the specified stream.
 *
 * Passed by polling_read to the process_func.
 */
typedef struct {
    stream_e stream;
    const char *buf;
    apr_size_t size;
} stream_output_t;

/**
 * Helper function for polling_read which handles events for a single descriptor.
 */
static apr_status_t polling_read_handle_descriptor(request_rec *r, const apr_pollfd_t *descriptor, apr_status_t (*process_func)(stream_output_t output, void *user_data), void *user_data, char *buf) {
    apr_status_t status;
    apr_size_t read_size;

    stream_e stream = (stream_e) descriptor->client_data;
    const char * desc_fd_name = stream_e_to_name(stream);

    ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Inspecting event on pipe %s with rtnevents=0x%x", desc_fd_name, descriptor->rtnevents);

    // Event type may be (at least) APR_POLLIN, APR_POLLERR, APR_POLLHUP, APR_POLLNVAL
    if ((descriptor->rtnevents & (APR_POLLIN|APR_POLLHUP|APR_POLLERR)) > 0) {
        // POLLIN: Data is available for reading.
        // POLLHUP: The other side of the pipe was closed, read should receive EOF.
        // POLLERR: An error is available, read should receive this error.

        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Event indicates that read() should be called");

        // read all available data and also check for EOF and other errors.
        do {
            ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Calling read() on pipe %s", desc_fd_name);

            read_size = READ_BUF_SIZE;
            status = apr_file_read(descriptor->desc.f, buf, &read_size);

            if (status == APR_SUCCESS) {
                ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "read() succeeded, returning bytes_read=%lu", read_size);
                status = process_func((stream_output_t) {stream, buf, read_size}, user_data);
                ap_assert(status == APR_SUCCESS);
                // TODO: how to handle this? Generally the process_func should avoid returning errors.
                // If it does return errors, should any further polling/processing stop, signalling that the process func can no longer process data?
                // In this case, the file descriptors should be read to their end, to prevent blocking by the child process.
            } else if (APR_STATUS_IS_EOF(status)) {
                // EOF means no more data will be available, remove fd from pollset, read_size is 0
                ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "read() returned EOF");
                return RSLV_SUCCESS_REMOVE_DESCRIPTOR;
            } else if (APR_STATUS_IS_EAGAIN(status)) {
                // No data available right now, continue polling, read_size is 0
                ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "read() returned EAGAIN");
            } else {
                // This is an unexpected error.
                // The error is logged and returned, thereby triggering killing of the process.
                // If this were be a transient error, reading again may work.
                ap_log_rerror(APLOG_MARK, APLOG_TRACE5, status, r, "read() returned unexpected error");
                ap_log_rerror(APLOG_MARK, APLOG_ERR, status, r, "Error while reading from %s pipe", desc_fd_name);
                return status;
            }

            // Perhaps the `while` condition should be replaced by a check for EAGAIN?
            // See epoll(7) for information of edge-triggered vs level-triggered, the
            // latter being semantically equivalent to poll(2).
            // Because of the level-triggered semantic in this case, it does
            // not seem to make a practical difference.
        } while (read_size == READ_BUF_SIZE);

    } else if ((descriptor->rtnevents & APR_POLLNVAL) > 0) {
        // This is unexpected, but if it occurs, remove the descriptor and continue.
        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Event has POLLNVAL");
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Unexpected POLLNVAL on event on %s pipe, rtnevents=0x%x, this is likely not a problem", desc_fd_name, descriptor->rtnevents);
        return RSLV_SUCCESS_REMOVE_DESCRIPTOR;
    } else if (descriptor->rtnevents == 0) {
        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Event has empty rtnevents");
    } else {
        // If this is a transient state, polling again may work.
        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Event has unexpected set of rtnevents");
        ap_log_rerror(APLOG_MARK, APLOG_ERR, APR_SUCCESS, r, "Unexpected set of rtnevents bits on %s pipe, rtnevents=0x%x", desc_fd_name, descriptor->rtnevents);
        return APR_EGENERAL;
    }

    return APR_SUCCESS;
}

/**
 * Perform a full polling read on stdout and stderr of a process, passing
 * received data to a custom function for processing.
 *
 * Through polling, both the stdout and stderr pipes of a running process are
 * read from concurrently in the current thread.
 * Reading continues until both pipes have been closed and all data has been
 * read.
 * Unexpected errors are returned immediately, aborting any further reading.
 *
 * @param r The request instance used to get the pool and for logging.
 * @param proc Running process with stdout and stderr initialised as pipes. Reads must not block, i.e. APR_WRITE_BLOCK should be configured.
 * @param process_func Function called for processing data received from one of the pipes.
 *                     It must always return APR_SUCCESS at the moment.
 *                     The function should block as little as possible to allow for fast polling.
 * @param user_data Pointer to user-defined data passed to process_func.
 */
static apr_status_t polling_read(request_rec *r, apr_proc_t *proc, apr_status_t (*process_func)(stream_output_t output, void *user_data), void *user_data) {
    apr_status_t status, ret_status = APR_SUCCESS;
    apr_pollset_t *pollset;
    apr_pollfd_t descriptor;

    char *buf = (char *) apr_palloc(r->pool, READ_BUF_SIZE);
    if (buf == NULL) return APR_ENOMEM;

    // Set up polling

    status = apr_pollset_create(&pollset, 2, r->pool, 0);
    ap_assert(status == APR_SUCCESS);

    descriptor = (apr_pollfd_t) {
        .p = r->pool,
        .desc_type = APR_POLL_FILE,
        .reqevents = APR_POLLIN, // POLLERR, POLLHUP, POLLNVAL also possible to be returned
        .rtnevents = 0,
        .desc = proc->out,
        .client_data = (void *) RSLV_STDOUT
    };
    status = apr_pollset_add(pollset, &descriptor);
    ap_assert(status == APR_SUCCESS);

    descriptor = (apr_pollfd_t) {
        .p = r->pool,
        .desc_type = APR_POLL_FILE,
        .reqevents = APR_POLLIN, // POLLERR, POLLHUP, POLLNVAL also possible to be returned
        .rtnevents = 0,
        .desc = proc->err,
        .client_data = (void *) RSLV_STDERR
    };
    status = apr_pollset_add(pollset, &descriptor);
    ap_assert(status == APR_SUCCESS);

    // Polling loop

    apr_int32_t num_events;
    const apr_pollfd_t *descriptors;
    apr_size_t active_descriptors = 2;

    ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Prepared pollset, starting polling loop");

    while (active_descriptors > 0) {
        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Calling poll() on %lu descriptors", active_descriptors);
        // Poll with a 100 ms timeout
        status = apr_pollset_poll(pollset, 100000, &num_events, &descriptors);
        if (APR_STATUS_IS_EINTR(status) || APR_STATUS_IS_TIMEUP(status)) {
            ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "poll() timed-out or was interrupted");
            // Poll again
            continue;
        } else if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_TRACE5, status, r, "poll() returned error");
            ap_log_rerror(APLOG_MARK, APLOG_ERR, status, r, "Error returned by poll()");
            ret_status = status;
            active_descriptors = 0;
            break;
        }

        ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "poll() succeeded, returning num_events=%d", num_events);

        for (int i = 0; i < num_events; ++i) {
            status = polling_read_handle_descriptor(r, &descriptors[i], process_func, user_data, buf);

            if (status == RSLV_SUCCESS_REMOVE_DESCRIPTOR) {
                ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Removing descriptor from pollset as indicated");
                status = apr_pollset_remove(pollset, &descriptors[i]);
                ap_assert(status == APR_SUCCESS);
                --active_descriptors;
            } else if (status != APR_SUCCESS) {
                // A severe problem occurred, stop polling immediately and return error.
                // The error was already logged in the inner function call.
                ret_status = status;
                active_descriptors = 0;
                break;
            }
        }
    }

    // Tear down of pollset

    ap_log_rerror(APLOG_MARK, APLOG_TRACE5, APR_SUCCESS, r, "Tearing down pollset");
    status = apr_pollset_destroy(pollset);
    ap_assert(status == APR_SUCCESS);

    return ret_status;
}

static apr_status_t nop_process_func(stream_output_t inp, void *state) {
}

/**
 * Kills a potentially running process in two steps - SIGTERM and then SIGKILL
 * after a timeout - while reading and discarding its stderr and stdout.
 *
 * Based on apr_pool.c@free_proc_chain.
 */
static apr_status_t friendly_kill_process(apr_proc_t *proc, apr_interval_time_t friendly_timeout) {
    // First check if the process is even running still
    if (apr_proc_wait(proc, NULL, NULL, APR_NOWAIT) == APR_CHILD_DONE)
        return APR_SUCCESS;

    // Send the process a SIGTERM so it can exit on its own.
    (void)apr_proc_kill(proc, SIGTERM);

    // Wait for the process to exit for friendly timeout
    apr_interval_time_t wait_total = 0, wait_interval = apr_time_from_msec(20);
    if (wait_interval > friendly_timeout)
        wait_interval = friendly_timeout;

    while (1) {
        apr_sleep(wait_interval);

        // TODO: perform a polling_read instead of sleeping,
        // this requires adapting polling_read
        //
        // apr_status_t status = polling_read(r, proc, nop_process_func, NULL);
        // if (APR_STATUS_IS_EAGAIN(status)) {
        //     // Timeout exceeded, need to poll again
        // } else if (status != APR_SUCCESS) {
        //     // Continue anyway, perhaps sleeping instead of polling
        //     // (either until the process died or for this interval)?
        // }
        // // if APR_SUCCESS, likely the process is dead

        if (apr_proc_wait(proc, NULL, NULL, APR_NOWAIT) == APR_CHILD_DONE)
            return APR_SUCCESS;

        wait_total += wait_interval;
        if (wait_total >= friendly_timeout)
            break;
        if (wait_interval <= apr_time_from_msec(250))
            wait_interval *= 2;
        else
            wait_interval = apr_time_from_msec(500);
        if (wait_total + wait_interval > friendly_timeout)
            wait_interval = friendly_timeout - wait_total;
    }

    // The process has not exited within the timeout interval.
    // Finally, perform a SIGKILL.

    (void)apr_proc_kill(proc, SIGKILL);

    (void)apr_proc_wait(proc, NULL, NULL, APR_WAIT);

    return APR_SUCCESS;
}

/**
 * Start and wait for a process while reading its stdout and stderr stream through a polling read.
 *
 * When the function returns the process has finished and "bookkeeping" in the kernel has been cleaned up.
 *
 * @param r Request context used for memory allocation and logging.
 * @param progname Program to be executed in the process.
 * @param args Arguments passed to the process.
 * @param process_func Function for processing chunks of stdout and stderr.
                       See polling_read() for more information.
 * @param user_data Pointer to any data which is passed to process_func.
 * @param exitcode The exitcode of the process is written to this pointer. The value can only be
 *                 be relied upon if the function returned successfully, although exitcode may
 *                 have been set regardless.
 */
static apr_status_t run_process(request_rec *r, const char *progname, const char *const *args, apr_status_t (*process_func)(stream_output_t output, void *user_data), void *user_data, int *exitcode) {
    apr_status_t status;
    apr_procattr_t *procattr;
    apr_proc_t *proc;
    apr_exit_why_e exitwhy;

    status = apr_procattr_create(&procattr, r->pool);
    if (status != APR_SUCCESS) return status;

    status = apr_procattr_io_set(
        procattr,
        APR_NO_PIPE,     // stdin: no file
        APR_WRITE_BLOCK, // stdout: read does not block (parent), write does (child)
        APR_WRITE_BLOCK  // stderr: read does not block (parent), write does (child)
    );
    if (status != APR_SUCCESS) return status;

    proc = apr_pcalloc(r->pool, sizeof(*proc));
    if (proc == NULL) return APR_ENOMEM;

    ap_log_rerror(APLOG_MARK, APLOG_TRACE3, APR_SUCCESS, r, "Starting process of program %s", progname);

    status = apr_proc_create(proc, progname, args, (const char * const *) NULL, procattr, r->pool);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while starting process");
        return status;
    }

    // Register the process to be killed when the pool is destroyed, i.e.
    // after request processing has finished.
    // This should not be necessary, but is a precaution if for some reason
    // the process survives this function.
    apr_pool_note_subprocess(r->pool, proc, APR_KILL_AFTER_TIMEOUT);

    ap_log_rerror(APLOG_MARK, APLOG_TRACE3, APR_SUCCESS, r, "Starting polling read of process' streams");
    status = polling_read(r, proc, process_func, user_data);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error during polling read of process' streams, killing process");
        (void)friendly_kill_process(proc, apr_time_from_sec(1));
        (void)apr_file_close(proc->out);
        (void)apr_file_close(proc->err);
        return status;
    }
    ap_log_rerror(APLOG_MARK, APLOG_TRACE3, APR_SUCCESS, r, "Polling read of process' streams finished");

    ap_log_rerror(APLOG_MARK, APLOG_TRACE3, APR_SUCCESS, r, "Waiting for process to exit");
    status = apr_proc_wait(proc, exitcode, &exitwhy, APR_WAIT);
    if (status != APR_CHILD_DONE) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while waiting for process to exit, killing process");
        (void)friendly_kill_process(proc, apr_time_from_sec(1));
        (void)apr_file_close(proc->out);
        (void)apr_file_close(proc->err);
        return status;
    }
    ap_log_rerror(APLOG_MARK, APLOG_TRACE3, APR_SUCCESS, r, "Process exited, collected information: exitcode=%d, exitwhy=0x%x", *exitcode, exitwhy);

    // Close pipes
    status = apr_file_close(proc->out);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_TRACE3, status, r, "Error while closing pipe stdout, ignoring");
    }
    status = apr_file_close(proc->err);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_TRACE3, status, r, "Error while closing pipe stderr, ignoring");
    }

    if (exitwhy != APR_PROC_EXIT) {
        const char *t1 = "";
        const char *t2 = "";
        const char *t3 = "";

        if ((exitwhy & APR_PROC_EXIT) > 0)
            t1 = "with regular exit";
        if ((exitwhy & APR_PROC_SIGNAL) > 0)
            t2 = "due to signal";
        if ((exitwhy & APR_PROC_SIGNAL_CORE) > 0)
            t3 = "due to coredump signal";

        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Unexpected process exit: Process exited %s %s %s exitcode/exitsignal=%d", t1, t2, t3, *exitcode);

        if ((exitwhy & APR_PROC_EXIT) == 0)
            return RSLV_UNCLEAN_PROCESS_DEATH;
    }

    return APR_SUCCESS;
}

/**
 * Join argv-style list of strings into a string/bytes with specified separators.
 *
 * @param pool Used for memory allocations.
 * @param args List of args, each string terminated by NULL char and the list terminated with a NULL pointer.
 * @param sep Separator string (actually bytes) between the arguments in the output.
 * @param sep_len Length of the separator.
 * @param out_len If not NULL, the length of the output is written here (including the NULL terminator).
 * @return Joined string (or byte-string if the sep contains bytes data) terminated by NULL.
 */
static char *join_argv(apr_pool_t *pool, const char *const *args, const unsigned char *sep, apr_size_t sep_len, apr_size_t *out_len) {
    apr_size_t alloc_len = 0;
    for (const char *const *arg = args; *arg != NULL; ++arg) {
        if (arg != args) // Not for the first arg
            alloc_len += sep_len;
        alloc_len += strlen(*arg);
    }

    alloc_len += 1; // Account for NULL terminator
    char *out = (char *) apr_palloc(pool, alloc_len);
    ap_assert(out != NULL);
    char *out_write_p = out;

    // Loop invariants: out has a trailing NULL byte, out_write_p points to the position in out
    // to be written to next.
    for (const char *const *arg = args; *arg != NULL; ++arg) {
        if (arg != args) {
            // Not for the first arg
            memcpy(out_write_p, sep, sep_len);
            out_write_p += sep_len;
        }
        out_write_p = apr_cpystrn(out_write_p, *arg, alloc_len-(out_write_p-out));
    }

    if (out_len != NULL)
        *out_len = alloc_len;

    return out;
}

/**
 * State shared between evaluate_process and its helper evaluate_process_handle_output.
 */
typedef struct {
    request_rec *r;

    const char *progname;

    int err_is_logged;
    char *err_buf;
    apr_size_t err_buf_capacity;
    apr_size_t err_buf_size;
    int err_skip_until_eol;
    int err_log_level;

    apr_size_t out_bytes_written;
    char *out_buf;
    apr_size_t out_buf_capacity;
    apr_size_t out_buf_size;
    int out_buf_full;
} evaluate_process_handle_output_state_t;

/**
 * Helper function for evaluate_process which is passed as process_func to run_process, handling
 * data written to stdout and stderr of the running process.
 */
static apr_status_t evaluate_process_handle_output(stream_output_t inp, void *void_state) {
    evaluate_process_handle_output_state_t *state = (evaluate_process_handle_output_state_t *) void_state;

    if (inp.stream == RSLV_STDERR && state->err_is_logged) {
        // Log line-wise

        const char *inp_buf = inp.buf;
        apr_size_t inp_buf_size = inp.size;

        while (inp_buf_size > 0) {
            char *eol_in_buf = memchr(inp_buf, '\n', inp_buf_size);

            apr_size_t buf_size_available = state->err_buf_capacity - state->err_buf_size;
            apr_size_t inp_size_consumed = (eol_in_buf == NULL)? inp_buf_size : eol_in_buf - inp_buf + 1; // Also consume LF char

            int overrun = 0, print_buf = 0;
            apr_size_t size_to_copy = 0;

            if (state->err_skip_until_eol) {
                // If in skip_until_eol mode

                if (eol_in_buf != NULL)
                    state->err_skip_until_eol = 0;
                // Process next chunk...
                continue;
            } else if (eol_in_buf == NULL) {
                // If EOL not found

                size_to_copy = inp_size_consumed;
                if (size_to_copy > buf_size_available) {
                    size_to_copy = buf_size_available;
                    overrun = 1;
                    state->err_skip_until_eol = 1;
                    print_buf = 1;
                }
            } else {
                // If EOL found

                print_buf = 1;

                size_to_copy = inp_size_consumed;
                if (size_to_copy > buf_size_available) {
                    size_to_copy = buf_size_available;
                    overrun = 1;
                }
            }

            memcpy(state->err_buf + state->err_buf_size, inp_buf, size_to_copy);
            state->err_buf_size += size_to_copy;

            if (print_buf) {
                if (overrun) {
                    if (state->err_buf_size >= 4) {
                        // "..." includes a null char at the end
                        memcpy(state->err_buf + state->err_buf_size - 4, "...", 4);
                    } else {
                        ap_assert(state->err_buf_capacity >= 1);
                        state->err_buf[0] = '\x00';
                        state->err_buf_size = 1;
                    }
                } else {
                    // If printing and not overrun, a newline char is the last char in the err_buf.

                    // Overwrite newline character with null char. Because of the newline, size >= 1.
                    state->err_buf[state->err_buf_size - 1] = '\x00';

                    if (state->err_buf_size >= 2 && state->err_buf[state->err_buf_size - 2] == '\r')
                        // If present, overwrite carriage return before newline with null char
                        state->err_buf[state->err_buf_size - 2] = '\x00';
                }

                // FUTR: Skip if err_buf contains empty line?

                ap_log_rerror(APLOG_MARK, state->err_log_level, APR_SUCCESS, state->r, "Process %s printed to stderr: %s", state->progname, state->err_buf);

                // Reset err_buf after print
                state->err_buf_size = 0;
            }

            inp_buf += inp_size_consumed;
            inp_buf_size -= inp_size_consumed;
        }
    } else if (inp.stream == RSLV_STDOUT) {
        state->out_bytes_written += inp.size;
        if (state->out_buf_full) {
        } else if (state->out_buf_size + inp.size > state->out_buf_capacity) {
            memcpy(state->out_buf + state->out_buf_size, inp.buf, state->out_buf_capacity - state->out_buf_size);
            state->out_buf_size = state->out_buf_capacity;
            state->out_buf_full = 1;
        } else {
            memcpy(state->out_buf + state->out_buf_size, inp.buf, inp.size);
            state->out_buf_size += inp.size;
        }
    }

    return APR_SUCCESS;
}

/**
 * Result of evaluating a process via evaluate_process.
 */
typedef struct {
    int exitcode;
    char *output;
    apr_size_t output_size;
} evaluate_process_result_t;

/**
 * Evaluates a process by starting a process of the program, waiting for it to exit and collecting its stdout.
 *
 * Data written to the stderr of the process is logged line-wise.
 *
 * @param r Request context used for memory allocation and logging.
 * @param progname Program to be executed in the process.
 * @param args Arguments passed to the process.
 * @param result Result of the process execution, must be allocated correctly by the caller.
 *               It contains the output as a raw buffer (always respect output_size) and the exitcode.
 *               The value can only be relied upon if the function returned successfully.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t evaluate_process(request_rec *r, const char *progname, const char *const *args, evaluate_process_result_t *result) {
    int exitcode;

    apr_size_t err_buf_capacity = 256;
    apr_size_t out_buf_capacity = 16 * 1024;

    char *err_buf = (char *) apr_palloc(r->pool, err_buf_capacity);
    if (err_buf == NULL) return APR_ENOMEM;
    char *out_buf = (char *) apr_palloc(r->pool, out_buf_capacity);
    if (out_buf == NULL) return APR_ENOMEM;

    evaluate_process_handle_output_state_t output_state = (evaluate_process_handle_output_state_t) {
        .r = r,

        .progname = progname,

        .err_is_logged = APLOG_R_IS_LEVEL(r, APLOG_WARNING),
        .err_buf = err_buf,
        .err_buf_capacity = err_buf_capacity,
        .err_buf_size = 0,
        .err_skip_until_eol = 0,
        .err_log_level = APLOG_WARNING,

        .out_bytes_written = 0,
        .out_buf = out_buf,
        .out_buf_capacity = out_buf_capacity,
        .out_buf_size = 0,
        .out_buf_full = 0
    };

    if (APLOGrdebug(r)) {
        // Join args for logging but skipping first arg, because it contains the program name
        char *joined_args = join_argv(r->pool, args + 1, " ", 1, NULL);
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Starting evaluation of process: %s %s", progname, joined_args);
    }

    apr_status_t status = run_process(r, progname, (const char * const*) args, evaluate_process_handle_output, (void *) &output_state, &exitcode);
    if (status != APR_SUCCESS) {
        return status;
    }

    if (output_state.err_is_logged && output_state.err_buf_size > 0) {
        // Print remaining err_buf content

        if (output_state.err_buf_size < output_state.err_buf_capacity) {
            output_state.err_buf[output_state.err_buf_size] = '\x00';
            output_state.err_buf_size += 1;
        } else {
            output_state.err_buf[output_state.err_buf_size - 1] = '\x00';
        }

        ap_log_rerror(APLOG_MARK, output_state.err_log_level, APR_SUCCESS, r, "Process %s printed to stderr: %s", progname, output_state.err_buf);

        // Reset err_buf after print
        output_state.err_buf_size = 0;
    }

    if (output_state.out_buf_full) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Output of process %s too long: Process wrote %lu bytes but only %lu bytes of capacity were allocated, discarding collected output", progname, output_state.out_bytes_written, output_state.out_buf_capacity);
        return RSLV_OUTPUT_OVERFLOW;
    }

    if (APLOGrdebug(r)) {
        // Create a str copy of the process' output for logging.
        char *out_str = apr_pstrmemdup(r->pool, output_state.out_buf, output_state.out_buf_size);
        if (out_str == NULL) return APR_ENOMEM;
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Process evaluation of %s finished (exit code %d), collected %lu bytes of output: %s", progname, exitcode, output_state.out_buf_size, out_str);
    }

    *result = (evaluate_process_result_t) {
        .exitcode = exitcode,
        .output = out_buf,
        .output_size = output_state.out_buf_size
    };

    return APR_SUCCESS;
}

/**
 * Utility for incrementally parsing an options string in in the format "key1=value1,key2=value2".
 *
 * Each invocation of the function will read the next key-value pair from input and update the input pointer.
 * This updated input pointer can then be passed to the next invocation of the function.
 *
 * @param pool Used for memory allocations.
 * @param input Pointer to a string of input to be parsed.
 *              Updated to point to the first char after the returned key-value pair.
 * @param key Updated to point to a string containing the next key.
 * @param value Updated to point to a string containing the next value.
 * @return APR_SUCCESS on success, APR_EOF if the end of input has been reached and an error status otherwise.
 */
static apr_status_t read_next_option(apr_pool_t *pool, const char **input, char **key, char **value) {
    const char *eq_sign, *value_start, *value_end_delim;

    if (**input == '\x00')
        return APR_EOF;

    eq_sign = strchr(*input, '=');
    if (eq_sign == NULL)
        return RSLV_PARAMS_PARSING_ERROR;

    *key = apr_pstrndup(pool, *input, eq_sign - *input);
    if (key == NULL) return APR_ENOMEM;

    value_start = eq_sign + 1;

    value_end_delim = strchr(value_start, ',');
    if (value_end_delim == NULL)
        value_end_delim = strchr(value_start, '\x00');

    *value = apr_pstrndup(pool, value_start, value_end_delim - value_start);
    if (value == NULL) return APR_ENOMEM;

    *input = value_end_delim;
    if (*value_end_delim != '\x00')
        ++(*input);

    return APR_SUCCESS;
}

/**
 * Parses a human-readable duration value into an apr_time_t.
 *
 * The supported format is: number . postfix
 * Where postfix can be: "ms" | "s" | "m" | "h", meaning milliseconds, seconds, minutes and hours.
 *
 * @param value Human-readable duration string.
 * @param duration The parsed duration is written here
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t parse_duration(const char *value, apr_time_t *duration) {
    const char *value_read_p = value;
    // For some reason strtoll requires a non-const-char endptr arg
    long int num = strtoll(value, (char **) &value_read_p, 10);
    if ((num == LLONG_MIN || num == LLONG_MAX) && apr_get_os_error() == APR_ERANGE) {
        return APR_ERANGE;
    }

    apr_time_t time_base;
    if (strcmp(value_read_p, "ms") == 0) {
        time_base = apr_time_from_msec(1);
    } else if (strcmp(value_read_p, "s") == 0) {
        time_base = apr_time_from_sec(1);
    } else if (strcmp(value_read_p, "m") == 0) {
        time_base = apr_time_from_sec(60);
    } else if (strcmp(value_read_p, "h") == 0) {
        time_base = apr_time_from_sec(60 * 60);
    } else {
        return RSLV_DURATION_INVALID_POSTFIX;
    }

    *duration = time_base * num;

    return APR_SUCCESS;
}

/**
 * Parses a params string part of the configuration directive ResolveByProcess and updates config with these options.
 *
 * @param pool Used for memory allocations.
 * @param input Input string to be parsed.
 * @param config resolve_config_t struct which is updated with the parsed configuration options.
 * @return NULL on success or an error message suitable for users otherwise.
 */
static const char *parse_resolve_config_params(apr_pool_t *pool, const char *input, resolve_config_t *config) {
    apr_status_t status;

    if (strcmp(input, "none") == 0)
        return NULL;

    const char *input_it = input;
    char *key, *value;

    while ((status = read_next_option(pool, &input_it, &key, &value)) == APR_SUCCESS) {
        if (strcmp(key, "stage") == 0) {
            stage_e stage;
            status = name_to_stage_e(value, &stage);
            if (status != APR_SUCCESS) return apr_psprintf(pool, "Error during parsing of \"stage\" param value (%s): %s", value, rslv_strerror_pool(pool, status));
            config->stage = stage;
        } else if (strcmp(key, "destination") == 0) {
            destination_e destination;
            status = name_to_destination_e(value, &destination);
            if (status != APR_SUCCESS) return apr_psprintf(pool, "Error during parsing of \"destination\" param value (%s): %s", value, rslv_strerror_pool(pool, status));
            config->destination = destination;
        } else if (strcmp(key, "cache") == 0) {
            if (strcmp(value, "none") != 0) {
                config->cache_name = value;
                config->caching_mode = RSLV_CM_CACHE_UNINITIALISED;
            }
        } else if (strcmp(key, "cache_ttl") == 0) {
            status = parse_duration(value, &config->cache_ttl);
            if (status != APR_SUCCESS) {
                return apr_psprintf(pool, "Error during parsing of \"cache_ttl\" param value (%s): %s", value, rslv_strerror_pool(pool, status));
            }
        } else {
            return apr_psprintf(pool, "Unknown parameter name: %s", key);
        }
    }

    if (status != APR_EOF) {
        const char *err = rslv_strerror_pool(pool, status);
        return apr_psprintf(pool, "Error while scanning input: %s", err);
    }

    return NULL;
}

static int integrate_config(apr_array_header_t *resolves_array, resolve_config_t *resolve_config) {
    int replaced = 0;

    for (apr_size_t i = 0; i < resolves_array->nelts; ++i) {
        resolve_config_t** other_config_ptr = &APR_ARRAY_IDX(resolves_array, i, resolve_config_t *);
        if (strcmp(resolve_config->unique_key, (*other_config_ptr)->unique_key) == 0) {
            *other_config_ptr = resolve_config;
            replaced = 1;
            break;
        }
    }

    if (!replaced)
        APR_ARRAY_PUSH(resolves_array, resolve_config_t *) = resolve_config;

    return replaced;
}

static apr_status_t merge_resolve_config_arrays(apr_pool_t *pool, apr_array_header_t **dest, const apr_array_header_t *a, const apr_array_header_t *b) {
    apr_array_header_t *resolves_array = apr_array_copy(pool, a);
    if (resolves_array == NULL) return APR_ENOMEM;

    for (apr_size_t i = 0; i < b->nelts; ++i) {
        resolve_config_t *resolve_config = APR_ARRAY_IDX(b, i, resolve_config_t *);
        integrate_config(resolves_array, resolve_config);
    }

    *dest = resolves_array;

    return APR_SUCCESS;
}

static const char *register_resolve_config(cmd_parms *parms, void *mconfig, int argc, char *const argv[]) {
    const char *err;
    module_dir_config_t *dir_config;

    err = ap_check_cmd_context(parms, NOT_IN_HTACCESS);
    if (err) {
        return err;
    }

    dir_config = (module_dir_config_t *) mconfig;
    ap_assert(dir_config != NULL);

    if (argc < 3) {
        return "ResolveByProcess requires at least three arguments, check documentation";
    }

    resolve_config_t *resolve_config = (resolve_config_t *) apr_pcalloc(parms->pool, sizeof(resolve_config_t));
    if (resolve_config == NULL) return "Failed to allocate memory";

    // Apply defaults
    resolve_config->stage = RSLV_POST_READ_REQUEST_MIDDLE;
    resolve_config->destination = RSLV_DESTINATION_REQENV;
    resolve_config->caching_mode = RSLV_CM_NONE;
    resolve_config->cache_ttl = 0;

    err = parse_resolve_config_params(parms->pool, argv[1], resolve_config);
    if (err) {
        return err;
    }

    resolve_config->var_name = apr_pstrdup(parms->pool, argv[0]);
    if (resolve_config->var_name == NULL) return "Failed to allocate memory";

    if (resolve_config->destination == RSLV_DESTINATION_FIELD) {
        field_e field;
        apr_status_t status = name_to_field_e(resolve_config->var_name, &field);
        if (status != APR_SUCCESS)
            return apr_psprintf(parms->pool, "Error converting the var name (%s) to a field: %s", resolve_config->var_name, rslv_strerror_pool(parms->pool, status));
        resolve_config->field = field;
    }

    resolve_config->unique_key = apr_pstrcat(parms->pool, destination_e_to_name(resolve_config->destination), ":", resolve_config->var_name, NULL);
    if (resolve_config->unique_key == NULL) return "Failed to allocate memory";

    resolve_config->prog_name = apr_pstrdup(parms->pool, argv[2]);
    if (resolve_config->prog_name == NULL) return "Failed to allocate memory";

    resolve_config->param_exprs_count = argc - 3;
    resolve_config->param_exprs = (ap_expr_info_t **) apr_pcalloc(parms->pool, sizeof(ap_expr_info_t*) * resolve_config->param_exprs_count);
    if (resolve_config->param_exprs == NULL) return "Failed to allocate memory";

    if (resolve_config->caching_mode != RSLV_CM_NONE && resolve_config->cache_ttl == 0) {
        return "Parameter cache is set and paramter cache_ttl must be set as well";
    }

    for (int i = 3; i < argc; ++i) {
        resolve_config->param_exprs[i - 3] = ap_expr_parse_cmd(parms, argv[i], AP_EXPR_FLAG_STRING_RESULT, &err, NULL);
        if (err != NULL) {
            return err;
        }
    }

    // Add to global list
    APR_ARRAY_PUSH(global_config.resolves, resolve_config_t *) = resolve_config;

    // Add to per-directory list in the correct stage
    switch (resolve_config->stage) {
        case RSLV_FIXUPS_MIDDLE:
            integrate_config(dir_config->fixups_middle_resolves, resolve_config);
            break;
        case RSLV_POST_READ_REQUEST_MIDDLE:
            integrate_config(dir_config->post_read_request_middle_resolves, resolve_config);
            break;
        case RSLV_CHECK_AUTHN_AFTER:
            integrate_config(dir_config->check_authn_last_resolves, resolve_config);
            break;
        case RSLV_CHECK_AUTHZ_AFTER:
            integrate_config(dir_config->check_authz_last_resolves, resolve_config);
            break;
        default:
            return apr_psprintf(parms->pool, "Unknown stage %s", stage_e_to_name(resolve_config->stage));
    }

    return NULL;
}

static const char *register_resolve_cache(cmd_parms *parms, void *mconfig, const char *cache_name, const char *provider_info) {
    const char *err;

    apr_array_header_t *caches = global_config.caches;

    err = ap_check_cmd_context(parms, GLOBAL_ONLY);
    if (err) {
        return err;
    }

    // Lookup the cache provider by name.

    // Both "name:args" and "name" are accepted
    const char *provider_name;
    const char *provider_args;
    const char *sep = strchr(provider_info, ':');
    if (sep != NULL) {
        provider_name = apr_pstrmemdup(parms->pool, provider_info, sep - provider_info);
        provider_args = sep + 1;
    } else {
        provider_name = provider_info;
        provider_args = NULL;
    }

    ap_socache_provider_t *provider = ap_lookup_provider(AP_SOCACHE_PROVIDER_GROUP, provider_name, AP_SOCACHE_PROVIDER_VERSION);
    if (provider == NULL) {
        return "Unknown cache provider name, check the socache documentation and make sure the cache module (mod_socache_...) is loaded.";
    }

    // Initialise the cache instance.

    cache_t *cache = (cache_t *) apr_pcalloc(parms->pool, sizeof(cache_t));

    cache->name = apr_pstrdup(parms->pool, cache_name);
    if (cache->name == NULL) return "Failed to allocate memory";

    cache->provider = provider;

    err = provider->create(&cache->instance, provider_args, parms->temp_pool, parms->pool);
    if (err) {
        return err;
    }

    // Add to the list of caches after checking for duplicate cache name.

    for (apr_size_t i = 0; i < caches->nelts; ++i) {
        cache_t *other_cache = APR_ARRAY_IDX(caches, i, cache_t *);
        if (strcmp(cache->name, other_cache->name) == 0) {
            return apr_psprintf(parms->pool, "Duplicate cache name passed to ResolveCache: %s", cache->name);
        }
    }

    APR_ARRAY_PUSH(caches, cache_t *) = cache;

    return NULL;
}

static const command_rec config_commands[] = {
    AP_INIT_TAKE_ARGV(
        "ResolveByProcess", register_resolve_config, NULL, RSRC_CONF|ACCESS_CONF,
        "Resolve a request variable or field by executing a process."
    ),
    AP_INIT_TAKE2(
        "ResolveCache", register_resolve_cache, NULL, RSRC_CONF,
        "Register a named cache backend which can be configured to cache the result of evaluations."
    ),
    { NULL }
};

static apr_status_t store_resolved_value(request_rec *r, const resolve_config_t *config, char *value) {
    switch (config->destination) {
        case RSLV_DESTINATION_REQENV:
            apr_table_setn(r->subprocess_env, config->var_name, value);
            return APR_SUCCESS;
        case RSLV_DESTINATION_FIELD:
            switch (config->field) {
                case RSLV_FIELD_REMOTE_USER:
                    r->user = value;
                    return APR_SUCCESS;
                default:
                    return RSLV_INVALID_ENUM_VALUE;
            }
        default:
            return RSLV_INVALID_ENUM_VALUE;
    }
}

/**
 * Evaluates all parameter expressions of config to build an argv list suitable for exec-ing a program.
 *
 * The program name is written to the first argument as this is convention.
 *
 * @param r Request for context for expr evaluation, memory allocations and logging.
 * @param config Resolve config which is evaluated.
 * @param args_out The pointer is updated to point to an argv-style array of strings terminated by a NULL pointer.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t evaluate_process_args_exprs(request_rec *r, const resolve_config_t *config, const char * const **args_out) {
    const char **args = (const char **) apr_pcalloc(r->pool, sizeof(const char *) * (config->param_exprs_count + 2));
    if (args == NULL) return APR_ENOMEM;

    args[0] = config->prog_name;
    args[config->param_exprs_count + 1] = NULL;

    for (int i = 0; i < config->param_exprs_count; ++i) {
        const char *err;
        args[i + 1] = ap_expr_str_exec(r, config->param_exprs[i], &err);
        if (err != NULL) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Error while executing expression for var %s: %s", config->var_name, err);
            return RSLV_EXPR_ERROR;
        }
    }

    *args_out = args;

    return APR_SUCCESS;
}

/**
 * Calculates and returns the length in bytes required to store the marshalled representation of result.
 */
static apr_size_t evaluate_process_result_marshalled_length(const evaluate_process_result_t *result) {
    return result->output_size + sizeof(apr_uint32_t) + sizeof(apr_uint32_t);
}

/**
 * Marshals result into a serialised/marshalled representation.
 *
 * evaluate_process_result_marshalled_length can be used to calculate how big the buffer should be.
 * If the buffer is too small, APR_ERANGE is returned.
 *
 * @param result Result to marshal.
 * @param buf Buffer to write to.
 * @param buf_cap Capacity of the buffer, the function will not write any more bytes.
 * @param bytes_written If not NULL, the number of bytes written by the function is stored here.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t evaluate_process_result_marshal(const evaluate_process_result_t *result, unsigned char *buf, apr_size_t buf_cap, apr_size_t *bytes_written) {
    // TODO: This limits output_size which is supposed to be 64 bit
    apr_uint32_t exitcode_net = htonl(result->exitcode);
    apr_uint32_t output_size_net = htonl(result->output_size);

    unsigned char *buf_write_p = buf;

    if (buf_cap - (buf_write_p - buf) < sizeof(exitcode_net))
        return APR_ERANGE;
    memcpy(buf_write_p, &exitcode_net, sizeof(exitcode_net));
    buf_write_p += sizeof(exitcode_net);

    if (buf_cap - (buf_write_p - buf) < sizeof(output_size_net))
        return APR_ERANGE;
    memcpy(buf_write_p, &output_size_net, sizeof(output_size_net));
    buf_write_p += sizeof(output_size_net);

    if (buf_cap - (buf_write_p - buf) < result->output_size)
        return APR_ERANGE;
    memcpy(buf_write_p, result->output, result->output_size);
    buf_write_p += result->output_size;

    if (bytes_written != NULL)
        *bytes_written = buf_write_p - buf;

    return APR_SUCCESS;
}

/**
 * Unmarshals result from a marshalled representation.
 *
 * If there is not enough data in the buffer, APR_ERANGE is returned.
 *
 * @param result Result to unmarshal to.
 * @param pool Pool for memory allocations.
 * @param buf Buffer to read from.
 * @param buf_size Size of the buffer, the function will not read any more bytes.
 * @param bytes_read If not NULL, the number of bytes read by the function is stored here.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t evaluate_process_result_unmarshal(evaluate_process_result_t *result, apr_pool_t *pool, const unsigned char *buf, apr_size_t buf_size, apr_size_t *bytes_read) {
    apr_uint32_t exitcode_net, output_size_net;

    const unsigned char *buf_read_p = buf;

    if (buf_size - (buf_read_p - buf) < sizeof(exitcode_net))
        return APR_ERANGE;
    memcpy(&exitcode_net, buf_read_p, sizeof(exitcode_net));
    buf_read_p += sizeof(exitcode_net);

    if (buf_size - (buf_read_p - buf) < sizeof(output_size_net))
        return APR_ERANGE;
    memcpy(&output_size_net, buf_read_p, sizeof(output_size_net));
    buf_read_p += sizeof(output_size_net);

    int exitcode = ntohl(exitcode_net);
    apr_size_t output_size = ntohl(output_size_net);

    if (buf_size - (buf_read_p - buf) < output_size)
        return APR_ERANGE;
    unsigned char *output = apr_pmemdup(pool, buf_read_p, output_size);
    if (output == NULL)
        return APR_ENOMEM;
    buf_read_p += output_size;

    *result = (evaluate_process_result_t) {
        .exitcode = exitcode,
        .output = output,
        .output_size = output_size,
    };

    if (bytes_read != NULL)
        *bytes_read = buf_read_p - buf;

    return APR_SUCCESS;
}

/**
 * Builds a cache key for a process execution performed by evaluate_process.
 *
 * The function presumes that the first argument (in args) is the program name.
 *
 * @param pool Pool for memory allocations.
 * @param progname Program name/path which should be executed.
 * @param args Arguments passed to the program.
 * @param key The key is written here, this is a bytes array.
 * @param size Size of the key in bytes.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t build_evaluate_process_cache_key(apr_pool_t *pool, const char *progname, const char *const *args, const unsigned char **key, apr_size_t *size) {
    const char *prefix = "process:";
    apr_size_t prefix_len = strlen(prefix);

    apr_size_t progname_len = strlen(progname);

    apr_size_t joined_args_len;
    // args + 1: Skip first arg (the program name again)
    const char *joined_args = join_argv(pool, args + 1, "\0", 1, &joined_args_len);
    // Ignore the trailing NULL
    --joined_args_len;

    apr_size_t buf_size = prefix_len + progname_len + 1 + joined_args_len;
    unsigned char *buf = apr_palloc(pool, buf_size);
    if (buf == NULL)
        return APR_ENOMEM;

    unsigned char *buf_write_p = buf;

    memcpy(buf_write_p, prefix, prefix_len);
    buf_write_p += prefix_len;

    memcpy(buf_write_p, progname, progname_len);
    buf_write_p += progname_len;

    *buf_write_p = '\0';
    ++buf_write_p;

    memcpy(buf_write_p, joined_args, joined_args_len);
    buf_write_p += joined_args_len;

    *key = buf;
    if (size != NULL)
        *size = buf_size;

    return APR_SUCCESS;
}

/**
 * Evaluates a resolve config by executing the configured process while first attempting to use a cache if configured.
 *
 * @param r Request for context, memory allocations and logging.
 * @param config Resolve config which is evaluated.
 * @param result The result of process evaluation (either from cache or by process execution) is written here.
 * @return APR_SUCCESS on success, an error status otherwise.
 */
static apr_status_t evaluate_resolve_config_by_process(request_rec *r, const resolve_config_t *config, evaluate_process_result_t *result) {
    apr_status_t status;

    cache_t *cache = NULL;
    const unsigned char *cache_key;
    apr_size_t cache_key_len;

    if (config->caching_mode == RSLV_CM_CACHE) {
        cache = config->cache;
    }

    const char * const *process_args;
    status = evaluate_process_args_exprs(r, config, &process_args);
    if (status != APR_SUCCESS) return status;

    if (cache != NULL) {
        status = build_evaluate_process_cache_key(r->pool, config->prog_name, process_args, &cache_key, &cache_key_len);
        if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while building cache key");
            return status;
        }

        // TODO: Additional logging

        apr_size_t buf_cap = 16 * 1024 + 64; // Max output size + a little bit for additional data
        apr_size_t buf_size = 0;
        unsigned char *buf = (unsigned char *) apr_palloc(r->pool, buf_cap);
        if (buf == NULL) return APR_ENOMEM;

        buf_size = buf_cap; // Must be set for retrieve call and will have the proper size thereafter
        status = cache->provider->retrieve(cache->instance, r->server, cache_key, cache_key_len, buf, (unsigned int *) &buf_size, r->pool);
        if (status == APR_NOTFOUND) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Process result not found in cache %s", config->cache->name);
        } else if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while retrieving value from cache");
            return status;
        } else {
            // Successfully got a value from cache

            // This ignores trailing bytes after the proper payload
            status = evaluate_process_result_unmarshal(result, r->pool, buf, buf_size, NULL);
            if (status != APR_SUCCESS) {
                ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while unmarshalling result from cached value");
                return status;
            }

            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Unmarshaled process result of %s from cache %s with exit code %d and %lu bytes of output: %s", config->prog_name, config->cache->name, result->exitcode, result->output_size, result->output);

            return APR_SUCCESS;
        }
    }

    status = evaluate_process(r, config->prog_name, (const char * const *) process_args, result);
    if (status != APR_SUCCESS) return status;

    if (cache != NULL) {
        apr_size_t buf_cap = evaluate_process_result_marshalled_length(result);
        apr_size_t buf_size = 0;
        unsigned char *buf = apr_palloc(r->pool, buf_cap);
        if (buf == NULL) return APR_ENOMEM;

        status = evaluate_process_result_marshal(result, buf, buf_cap, &buf_size);
        if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while marshalling result for caching");
            return status;
        }

        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Storing result in cache %s with TTL %ld.%06ld s", config->cache->name, apr_time_sec(config->cache_ttl), apr_time_usec(config->cache_ttl));

        apr_time_t expiry = apr_time_now() + config->cache_ttl;
        status = cache->provider->store(cache->instance, r->server, cache_key, cache_key_len, expiry, buf, buf_size, r->pool);
        if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, status, r, "Error while storing value in cache");
            return status;
        }
    }

    return APR_SUCCESS;
}

static apr_status_t resolve_resolve_config_and_store_result(request_rec *r, const resolve_config_t *config) {
    apr_status_t status;

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Resolving var %s by process %s", config->var_name, config->prog_name);

    evaluate_process_result_t result;

    status = evaluate_resolve_config_by_process(r, config, &result);
    if (status != APR_SUCCESS) return status;

    if (result.exitcode != 0) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Error exit code %d from process %s for var %s", result.exitcode, config->prog_name, config->var_name);
        return RSLV_ERROR_PROCESS_EXIT_CODE;
    }

    char *value = apr_pstrndup(r->pool, result.output, result.output_size);
    if (value == NULL) return APR_ENOMEM;

    str_trim_in_place(value);

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Resolved var %s by process %s to: %s", config->var_name, config->prog_name, value);

    status = store_resolved_value(r, config, value);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Error while storing value in var %s", config->var_name);
        return status;
    }

    return APR_SUCCESS;
}

static apr_status_t resolve_authz_resolve_config(request_rec *r, const resolve_config_t *config, authz_status *authz_result) {
    apr_status_t status;

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Resolving authz (Require) condition %s by process %s", config->var_name, config->prog_name);

    evaluate_process_result_t result;

    status = evaluate_resolve_config_by_process(r, config, &result);
    if (status != APR_SUCCESS) return status;

    if (result.exitcode != 0) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, APR_SUCCESS, r, "Error exit code %d from process %s for authz (Require) condition %s", result.exitcode, config->prog_name, config->var_name);
        return RSLV_ERROR_PROCESS_EXIT_CODE;
    }

    char *value = apr_pstrndup(r->pool, result.output, result.output_size);
    if (value == NULL) return APR_ENOMEM;

    str_trim_in_place(value);

    status = name_to_authz_status(value, authz_result);
    if (status != APR_SUCCESS) return status;
    if (*authz_result == AUTHZ_GENERAL_ERROR) {
        // AUTHZ_GENERAL_ERROR must not be written by the process, which should instead exit with a non-zero exit code.
        return RSLV_INVALID_ENUM_VALUE;
    }

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, r, "Resolved authz (Require) condition %s by process %s to: %s", config->var_name, config->prog_name, authz_status_to_name(*authz_result));

    return APR_SUCCESS;
}

static int resolve_all_for_hook(request_rec *r, const apr_array_header_t *resolves) {
    for (apr_size_t i = 0; i < resolves->nelts; ++i) {
        resolve_config_t *resolve_config = APR_ARRAY_IDX(resolves, i, resolve_config_t *);

        apr_status_t status = resolve_resolve_config_and_store_result(r, resolve_config);
        if (status != APR_SUCCESS) {
            // Get and log error message for error status.
            const char *error_str = rslv_strerror_pool(r->pool, status);
            ap_log_rerror(APLOG_MARK, APLOG_ERR, APR_SUCCESS, r, "Error while resolving var %s: %s", resolve_config->var_name, error_str);
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return DECLINED;
}

static void *create_server_config(apr_pool_t *pool, server_rec *s) {
    module_server_config_t *config = (module_server_config_t *) apr_pcalloc(pool, sizeof(module_server_config_t));
    ap_assert(config != NULL);

    return config;
}

static void *create_dir_config(apr_pool_t *pool, char *context) {
    module_dir_config_t *config = (module_dir_config_t *) apr_pcalloc(pool, sizeof(module_dir_config_t));
    ap_assert(config != NULL);

    config->fixups_middle_resolves = apr_array_make(pool, 0, sizeof(resolve_config_t *));
    ap_assert(config->fixups_middle_resolves != NULL);
    config->post_read_request_middle_resolves = apr_array_make(pool, 0, sizeof(resolve_config_t *));
    ap_assert(config->post_read_request_middle_resolves != NULL);
    config->check_authn_last_resolves = apr_array_make(pool, 0, sizeof(resolve_config_t *));
    ap_assert(config->check_authn_last_resolves != NULL);
    config->check_authz_last_resolves = apr_array_make(pool, 0, sizeof(resolve_config_t *));
    ap_assert(config->check_authz_last_resolves != NULL);

    return config;
}

static void *merge_dir_config(apr_pool_t *pool, void *BASE, void *ADD) {
    module_dir_config_t *base = (module_dir_config_t *) BASE;
    module_dir_config_t *add = (module_dir_config_t *) ADD;
    module_dir_config_t *config = (module_dir_config_t *) apr_pcalloc(pool, sizeof(module_dir_config_t));
    ap_assert(config != NULL);

    merge_resolve_config_arrays(pool, &config->fixups_middle_resolves, base->fixups_middle_resolves, add->fixups_middle_resolves);
    merge_resolve_config_arrays(pool, &config->post_read_request_middle_resolves, base->post_read_request_middle_resolves, add->post_read_request_middle_resolves);
    merge_resolve_config_arrays(pool, &config->check_authn_last_resolves, base->check_authn_last_resolves, add->check_authn_last_resolves);
    merge_resolve_config_arrays(pool, &config->check_authz_last_resolves, base->check_authz_last_resolves, add->check_authz_last_resolves);

    return config;
}

static int init_config(apr_pool_t *p, apr_pool_t *plog, apr_pool_t *ptemp) {
    global_config.caches = apr_array_make(p, 0, sizeof(cache_t *));
    if (global_config.caches == NULL) {
        ap_log_perror(APLOG_MARK, APLOG_ERR, APR_ENOMEM, plog, "Failed to allocate memory");
        return RSLV_HOOK_ERROR;
    }

    global_config.resolves = apr_array_make(p, 0, sizeof(resolve_config_t *));
    if (global_config.resolves == NULL) {
        ap_log_perror(APLOG_MARK, APLOG_ERR, APR_ENOMEM, plog, "Failed to allocate memory");
        return RSLV_HOOK_ERROR;
    }

    return OK;
}

static apr_status_t search_and_set_cache_from_global_config(server_rec *s, resolve_config_t *config) {
    if (config->caching_mode == RSLV_CM_CACHE_UNINITIALISED) {
        for (apr_size_t i = 0; i < global_config.caches->nelts; ++i) {
            cache_t *cache = APR_ARRAY_IDX(global_config.caches, i, cache_t *);
            if (strcmp(config->cache_name, cache->name) == 0) {
                ap_log_error(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, s, "found cache %s for config %lx", config->cache_name, (unsigned long int) config);
                config->cache = cache;
                config->caching_mode = RSLV_CM_CACHE;
                break;
            }
        }

        if (config->caching_mode != RSLV_CM_CACHE) {
            // No cache found
            ap_log_error(APLOG_MARK, APLOG_ERR, APR_SUCCESS, s, "Error in server configuration: unknown cache %s", config->cache_name);
            return RSLV_UNKNOWN_CACHE;
        }
    } else if (config->caching_mode == RSLV_CM_CACHE) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, s, "cache %s for config %lx already present", config->cache_name, (unsigned long int) config);
    }

    return APR_SUCCESS;
}

static int finalise_config(apr_pool_t *p, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
    // s is an array of server_rec structs

    // Initialise cache instances.

    apr_array_header_t *caches = global_config.caches;

    const struct ap_socache_hints hints = (struct ap_socache_hints) {
        .avg_id_len = 50,
        .avg_obj_size = 20,
        .expiry_interval = apr_time_sec(300),
    };

    for (apr_size_t i = 0; i < caches->nelts; ++i) {
        cache_t *cache = APR_ARRAY_IDX(caches, i, cache_t *);

        ap_log_error(APLOG_MARK, APLOG_DEBUG, APR_SUCCESS, s, "Initialising cache %s", cache->name);

        apr_status_t status = cache->provider->init(cache->instance, "mod_resolve", &hints, s, p);
        if (status != APR_SUCCESS) {
            ap_log_error(APLOG_MARK, APLOG_ERR, status, s, "Error while initialising cache %s", cache->name);
            return RSLV_HOOK_ERROR;
        }
    }

    // Search and set the cache configured for each resolve config.

    apr_array_header_t *resolves = global_config.resolves;

    for (apr_size_t i = 0; i < resolves->nelts; ++i) {
        resolve_config_t *resolve_config = APR_ARRAY_IDX(resolves, i, resolve_config_t *);

        apr_status_t status = search_and_set_cache_from_global_config(s, resolve_config);
        if (status != APR_SUCCESS) {
            return RSLV_HOOK_ERROR;
        }
    }

    return OK;
}

static int resolve_all_fixups_middle(request_rec *r) {
    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &resolve_module);

    return resolve_all_for_hook(r, dir_config->fixups_middle_resolves);
}

static int resolve_all_post_read_request_middle(request_rec *r) {
    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &resolve_module);

    return resolve_all_for_hook(r, dir_config->post_read_request_middle_resolves);
}

static int hijack_check_authn(request_rec *r) {
    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &resolve_module);

    if (dir_config->check_authn_last_resolves->nelts > 0 && apr_table_get(r->notes, RSLV_AUTHN_HIJACK_STATE) != hijack_state_hijacked) {
        int hook_status, return_status = DECLINED;

        apr_table_setn(r->notes, RSLV_AUTHN_HIJACK_STATE, hijack_state_hijacked);

        hook_status = ap_run_check_user_id(r);
        if (hook_status == OK)
            return_status = hook_status;
        else if (hook_status != DECLINED)
            return hook_status;

        hook_status = resolve_all_for_hook(r, dir_config->check_authn_last_resolves);
        if (hook_status != DECLINED)
            return hook_status;

        return return_status;
    }

    return DECLINED;
}

static int hijack_check_authz(request_rec *r) {
    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &resolve_module);

    if (dir_config->check_authz_last_resolves->nelts > 0 && apr_table_get(r->notes, RSLV_AUTHZ_HIJACK_STATE) != hijack_state_hijacked) {
        int hook_status, return_status = DECLINED;

        apr_table_setn(r->notes, RSLV_AUTHZ_HIJACK_STATE, hijack_state_hijacked);

        hook_status = ap_run_auth_checker(r);
        if (hook_status == OK)
            return_status = hook_status;
        else if (hook_status != DECLINED)
            return hook_status;

        hook_status = resolve_all_for_hook(r, dir_config->check_authz_last_resolves);
        if (hook_status != DECLINED)
            return hook_status;

        return return_status;
    }

    return DECLINED;
}

#define RSLV_AUTHZ_REQUIRE_LINE_MAX_ARGC 64

static const char *authz_by_process_provider_parse_line(cmd_parms *parms, const char *require_line, const void **parsed_require_line) {
    const char *err;

    // Tokenise line into words while respecting quotes.

    // From HTTPD server/config.c:900
    char *argv[RSLV_AUTHZ_REQUIRE_LINE_MAX_ARGC];
    int argc = 0;

    const char *require_line_iterator = require_line;

    do {
        char *word;
        word = ap_getword_conf(parms->pool, &require_line_iterator);
        if (*word == '\0' && *require_line_iterator == '\0') {
            break;
        }
        argv[argc] = word;
        argc++;
    } while (argc < RSLV_AUTHZ_REQUIRE_LINE_MAX_ARGC && *require_line_iterator != '\0');

    if (*require_line_iterator != '\0')
        return "Too many arguments in Require by-process directive";

    // Create a resolve_config_t from the tokenised line.
    // FUTR: Add another function analogously parse_resolve_config_params for building a config specifically for authz
    // FUTR: Refactor creation of resolve_config to reduce duplicating this code

    // From @register_resolve_config :1230
    resolve_config_t *resolve_config = (resolve_config_t *) apr_pcalloc(parms->pool, sizeof(resolve_config_t));
    if (resolve_config == NULL) return "Failed to allocate memory";

    // Apply defaults
    resolve_config->caching_mode = RSLV_CM_NONE;
    resolve_config->cache_ttl = 0;

    // There are no defaults in this use case, so start parsing of params right away
    err = parse_resolve_config_params(parms->pool, argv[0], resolve_config);
    if (err) {
        return err;
    }

    const char *name = apr_psprintf(parms->pool, "by-process@%s:%d", parms->directive->filename, parms->directive->line_num);
    resolve_config->var_name = name;
    resolve_config->unique_key = name;

    resolve_config->prog_name = apr_pstrdup(parms->pool, argv[1]);
    if (resolve_config->prog_name == NULL) return "Failed to allocate memory";

    resolve_config->param_exprs_count = argc - 2;
    resolve_config->param_exprs = (ap_expr_info_t **) apr_pcalloc(parms->pool, sizeof(ap_expr_info_t*) * (resolve_config->param_exprs_count));
    if (resolve_config->param_exprs == NULL) return "Failed to allocate memory";

    if (resolve_config->caching_mode != RSLV_CM_NONE && resolve_config->cache_ttl == 0) {
        return "Parameter cache is set and parameter cache_ttl must be set as well";
    }

    for (int i = 2; i < argc; ++i) {
        resolve_config->param_exprs[i - 2] = ap_expr_parse_cmd(parms, argv[i], AP_EXPR_FLAG_STRING_RESULT, &err, NULL);
        if (err != NULL) {
            return err;
        }
    }

    // Store the resolve_config_t.

    // Add to global list
    APR_ARRAY_PUSH(global_config.resolves, resolve_config_t *) = resolve_config;
    // Store in the opaque parsed_require_line pointer
    *parsed_require_line = resolve_config;

    return NULL;
}

static authz_status authz_by_process_provider_check_authorization(request_rec *r, const char *require_line, const void *parsed_require_line) {
    const resolve_config_t *resolve_config = (const resolve_config_t *) parsed_require_line;

    authz_status result;
    apr_status_t status = resolve_authz_resolve_config(r, resolve_config, &result);
    if (status != APR_SUCCESS) {
        // Get and log error message for error status.
        const char *error_str = rslv_strerror_pool(r->pool, status);
        ap_log_rerror(APLOG_MARK, APLOG_ERR, APR_SUCCESS, r, "Error while resolving authz (Require) condition %s: %s", resolve_config->var_name, error_str);
        return AUTHZ_GENERAL_ERROR;
    }

    return result;
}

static const authz_provider authz_by_process_provider = (authz_provider) {
    .check_authorization = &authz_by_process_provider_check_authorization,
    .parse_require_line = &authz_by_process_provider_parse_line,
};

static void register_hooks(apr_pool_t *pool) {
    ap_hook_pre_config(init_config, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_post_config(finalise_config, NULL, NULL, APR_HOOK_MIDDLE);

    ap_hook_fixups(resolve_all_fixups_middle, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_post_read_request(resolve_all_post_read_request_middle, NULL, NULL, APR_HOOK_MIDDLE);

    // Regarding the last parameter for authn and authz hooks and providers:
    //  * AP_AUTH_INTERNAL_PER_CONF: Access control hooks are invoked only
    //    once when a request performs subrequests with identical access
    //    control configuration.
    //  * AP_AUTH_INTERNAL_PER_URI: Access control hooks are invoked on every
    //    request. If any module registers a hook function with this value,
    //    this behaviour is triggered.
    //
    // So here AP_AUTH_INTERNAL_PER_CONF is specified, which is also used by
    // all in-tree modules as well as mod_auth_openidc and mod_oauth2.

    ap_hook_check_authn(hijack_check_authn, NULL, NULL, APR_HOOK_REALLY_FIRST, AP_AUTH_INTERNAL_PER_CONF);
    ap_hook_check_authz(hijack_check_authz, NULL, NULL, APR_HOOK_REALLY_FIRST, AP_AUTH_INTERNAL_PER_CONF);

    ap_register_auth_provider(pool, AUTHZ_PROVIDER_GROUP, "by-process", AUTHZ_PROVIDER_VERSION, &authz_by_process_provider, AP_AUTH_INTERNAL_PER_CONF);
}

// TODO: log level for stderr messages

// TODO: How should users identify/recognise an individual directive: Either via var name, file and line number or full line given in config

// TODO: Rename special empty-params word from "none" to "default" or "defaults"?

// TODO: In the authz provider, how to distinguish a NULL username from an empty string username? Is the distinction relevant?

AP_DECLARE_MODULE(resolve) = {
    STANDARD20_MODULE_STUFF,
    create_dir_config,
    merge_dir_config,
    create_server_config,
    NULL,
    config_commands,
    register_hooks
};
