%global upstream_version 0.2.0
%global package_release 1

Summary:    mod_resolve for Apache HTTP Server
Name:       httpd-mod-resolve
Version:    %( echo %upstream_version | sed -E 's/-(beta|alpha|rc)/~\1/i' | tr '-' '.' )
Release:    %{package_release}%{?dist}
URL:        https://codebase.helmholtz.cloud/kit-scc-sdm/onlinestorage/httpd-webdav/mod-resolve
License:    ASL 2.0
Group:      System Environment/Daemons

Source0:    mod_resolve.tar.gz
Source1:    README.md

BuildRequires:  gcc
BuildRequires:  make
# According to RHBZ #1059143, httpd-2.4.6-21 has some backported patches
%if 0%{?el7}
BuildRequires:  httpd-devel >= 2.4.6-21.el7
Requires:       httpd >= 2.4.6-21.el7
%else
BuildRequires:  httpd-devel >= 2.4.7
Requires:       httpd >= 2.4.7
%endif
BuildRequires:  which


%description
The Apache HTTP Server is a powerful, efficient, and extensible web server.

This package contain mod_resolve which allows to resolve values and perform
other logic through external programs during request processing.

%prep
%setup -q -n mod_resolve


%build
# Do not need configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

install -m 644 %{SOURCE1} .
install -D .libs/mod_resolve.so %{buildroot}/%{_httpd_moddir}/mod_resolve.so
install -d %{buildroot}/%{_httpd_modconfdir}/

cat > %{buildroot}/%{_httpd_modconfdir}/resolve.load << EOF
# mod_resolve allows to resolve values through external programs.
#LoadModule resolve_module modules/mod_resolve.so
EOF


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README.md
%{_httpd_moddir}/mod_resolve.so
%config(noreplace) %{_httpd_modconfdir}/resolve.load

%changelog
* Sat Jun 24 2023 Paul Skopnik <paul.skopnik@kit.edu> - 0.2.0-1
- First release
